
#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>
#import <WatchConnectivity/WatchConnectivity.h>
#import <Uno-iOS/Context.h>
#import <Uno-iOS/Uno-iOS.h>
#import <Uno-iOS/AppDelegate.h>
#import <string>
@{Fuse.Platform.Lifecycle:IncludeDirective}
@{AppleWatchImpl:IncludeDirective}

@interface uAppDelegate (Watch) <WCSessionDelegate>

- (void)sessionWatchStateDidChange:(WCSession *)session;
- (void)sessionDidBecomeInactive:(WCSession *)session;
- (void)session:(WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(NSError *)error;
- (void)sessionDidDeactivate:(WCSession *)session;
- (void)session:(WCSession *)session didReceiveApplicationContext:(NSDictionary<NSString *,id> *)applicationContext;
- (void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *, id> *)message replyHandler:(void(^)(NSDictionary<NSString *, id> *replyMessage))replyHandler;
- (void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *, id> *)message;

@end
