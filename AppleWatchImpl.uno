using Uno;
using Uno.Collections;
using Fuse;
using Uno.Threading;

public extern(!Mobile || Android) class AppleWatchImpl
{

	public static void sendMessageToWatch(Context c,object[] args){
		debug_log("AppleWatch only working on iOS");
	}

	public static void updateAppContext (Context c,object[] args) {
		debug_log("AppleWatch only working on iOS");
	}

}
