
#import "uObjC.Foreign.h"
#import <Uno/Uno.h>
#import <ObjC.Object.h>
#import <WatchManager.hh>

@implementation WatchManager

 WCSession* _session;

- (void)initWatchSession{
  if ([WCSession isSupported]) {
   _session = [WCSession defaultSession];
   [ _session activateSession];
  }
}

- (void)sendMessageToWatch:(NSString*)messageJsonString{
  NSLog(@"sendMessageToWatch chamada");
  NSError *jsonError;
  NSData *objectData = [messageJsonString dataUsingEncoding:NSUTF8StringEncoding];
  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
  [ _session sendMessage:json replyHandler:nil errorHandler:nil];

}

- (void)updateAppContext:(NSString*)contextJsonString{
  NSLog(@"update App Context chamada");
  NSError *jsonError;
  NSData *objectData = [contextJsonString dataUsingEncoding:NSUTF8StringEncoding];
  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
  [ _session updateApplicationContext:json error:nil];
}

@end
