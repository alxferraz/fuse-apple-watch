#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>
#import <WatchConnectivity/WatchConnectivity.h>
#import <string>

@{AppleWatchImpl:IncludeDirective}

@interface WatchManager : NSObject
- (void)initWatchSession;
- (void)sendMessageToWatch:(NSString*)messageJsonString;
- (void)updateAppContext:(NSString*)contextJsonString;
@end
