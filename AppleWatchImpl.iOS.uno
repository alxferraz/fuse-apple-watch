using Uno;
using Fuse;
using Uno.Compiler.ExportTargetInterop;
using Uno.Threading;

[Require("Xcode.Framework", "WatchKit")]
[Require("Xcode.Framework", "WatchConnectivity")]

[ForeignInclude(Language.ObjC, "WatchManager.hh")]


public extern(iOS) class AppleWatchImpl {

	public static void messageReceivedFromWatch(string message)
	{
		AppleWatch._messageReceived.RaiseAsync(AppleWatch._messageReceived.ThreadWorker,message);
	}

	public static void contextReceivedFromWatch(string context)
	{
		AppleWatch._didReceiveContext.RaiseAsync(AppleWatch._didReceiveContext.ThreadWorker,context);
	}

  [Foreign(Language.ObjC)]
	public static void _updateAppContext (string context)
	@{
		WatchManager* x = [[WatchManager alloc] init];
		[x updateAppContext: context];
	@}

	[Foreign(Language.ObjC)]
	public static void _sendMessageToWatch (string message)
	@{
		WatchManager* x = [[WatchManager alloc] init];
		[x sendMessageToWatch: message];
	@}

}
