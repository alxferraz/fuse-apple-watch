using Uno;
using Uno.UX;
using Uno.Threading;
using Fuse;
using Fuse.Scripting;
using Fuse.Reactive;
using Uno.Permissions;
using Uno.Compiler.ExportTargetInterop;
using Fuse.Controls.Native.Android;


[extern(iOS) Require("Xcode.Framework", "WatchKit")]
[extern(iOS) Require("Xcode.Framework", "WatchConnectivity")]

[UXGlobalModule]
public class AppleWatch : NativeModule {

	static readonly AppleWatch _instance;
	public static NativeEvent _messageReceived;
	public static NativeEvent _resultEvent;

	public AppleWatch()
	{
		//if (_instance != null) return;
		_instance = this;
		Uno.UX.Resource.SetGlobalKey(_instance, "AppleWatch");
		AddMember(new NativeFunction("sendMessageToWatch", (NativeCallback)sendMessageToWatch));
		AddMember(new NativeFunction("updateAppContext", (NativeCallback)updateAppContext));

		_messageReceived = new NativeEvent("onMessageReceived");
		AddMember(_messageReceived);

		_didReceiveContext = new NativeEvent("didReceiveContext");
		AddMember(_didReceiveContext);

	}

	string sendMessageToWatch (Context c,object[] args)
 	{
 		 AppleWatchImpl._sendMessageToWatch(args[0].ToString());
		 return "";
 	}

	string updateAppContext (Context c,object[] args)
 	{
 		 AppleWatchImpl._updateAppContext(args[0].ToString());
		 return "";
 	}



}
