
#import "uObjC.Foreign.h"
#import <Uno/Uno.h>
#import <Fuse.Platform.SystemUI.h>
#import <ObjC.Object.h>
#import <AppDelegateWatch.hh>

@implementation uAppDelegate (Watch)

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    uAutoReleasePool pool;

    if(WCSession.isSupported){
        WCSession* session = WCSession.defaultSession;
        session.delegate = self;
        [session activateSession];
      }

    return YES;
}


- (void)sessionWatchStateDidChange:(nonnull WCSession *)session{
    NSLog(@"WC statedidchange");


        if(session.reachable){
            NSLog(@"session.reachable");
        }

        if(session.paired){
            NSLog(@"WC session paired");
            if(session.isWatchAppInstalled){

                if(session.watchDirectoryURL != nil){

                }
            }
       }

}

- (void)sessionDidBecomeInactive:(nonnull WCSession *)session {

}

- (void)session:(nonnull WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(nullable NSError *)error {
    if( activationState == WCSessionActivationStateActivated) {
                  NSLog(@"iPhone WKit session Activated");
              }else if (activationState == WCSessionActivationStateInactive) {
                  NSLog(@"iPhone WKit Inactive");
              }else if (activationState == WCSessionActivationStateNotActivated) {
                  NSLog(@"iPhone WKit NotActivated");
              }
}

- (void)sessionDidDeactivate:(nonnull WCSession *)session {

}


- (void)session:(nonnull WCSession *)session didReceiveApplicationContext:(NSDictionary<NSString *,id> *)applicationContext{
    NSLog(@"WC didreceivcontext");
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:applicationContext options:0 error:&error];

    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        @{AppleWatchImpl.contextReceivedFromWatch(string):Call(jsonString)};
    }


}

- (void)session:(nonnull WCSession *)session didReceiveMessage:(NSDictionary<NSString *, id> *)message replyHandler:(void(^)(NSDictionary<NSString *, id> *replyMessage))replyHandler{
    if(message){
        NSLog(@"WC didreceivemessage with reply handler");
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:message options:0 error:&error];

        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

            @{AppleWatchImpl.messageReceivedFromWatch(string):Call(jsonString)};
        }

    }
}

- (void)session:(nonnull WCSession *)session didReceiveMessage:(NSDictionary<NSString *, id> *)message {
    if(message){
        NSLog(@"WC didreceivemessage");
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:message options:0 error:&error];

        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

            @{AppleWatchImpl.messageReceivedFromWatch(string):Call(jsonString)};
        }


    }
}

@end
